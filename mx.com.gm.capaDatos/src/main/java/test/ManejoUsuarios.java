package test;

import datos.UsuarioDao;
import datos.UsuarioDaoJDBC;
import domain.UsuarioDTO;
import java.sql.SQLException;
import java.util.List;


public class ManejoUsuarios {
    public static void main(String[] args) throws SQLException {
        UsuarioDao usuarioDao = new UsuarioDaoJDBC();
        
        List<UsuarioDTO> usuarios = usuarioDao.select();
        for(UsuarioDTO usuario: usuarios) {
            System.out.println("Usuario: " + usuario);
        }
        
//        Usuario usuario1 = new Usuario();
//        usuario1.setUsername("Max Holloway");
//        usuario1.setPassword("347");
//        System.out.println(usuarioJdbc.insert(usuario1));
        
//        Usuario usuario = new Usuario(2, "Zabit Magomedsharipov", "667");
//        usuarioJdbc.update(usuario);
        
//        usuarioJdbc.delete(new Usuario(3));
    }
}
