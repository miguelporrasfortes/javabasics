
import java.util.Scanner;

public class Grades {

    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);
        
        int grade;
        
        System.out.println("Please, enter your grade between 0 and 10");
        grade = Integer.parseInt(scanner.nextLine());
        if (grade >= 9 && grade <= 10 ) {
            System.out.println("Grade = A");
        }
        else if(grade >= 8 && grade < 9 ) {
            System.out.println("Grade = B");
        }
        else if (grade >= 7 && grade < 8) {
            System.out.println("Grade = C");
        }
        else if (grade >= 6 && grade < 7) {
            System.out.println("Grade = D");
        }
        else if (grade >= 0 && grade < 6) {
            System.out.println("Grade = F");
        }
        else {
            System.out.println("Valor desconocido" );
        }
    }
}

//El objetivo del ejercicio es crear un sistema de calificaciones, como sigue:
//
//El usuario proporcionará un valor entre 0 y 10.
//
//Si está entre 9 y 10: imprimir una A
//
//Si está entre 8 y menor a 9: imprimir una B
//
//Si está entre 7 y menor a 8: imprimir una C
//
//Si está entre 6 y menor a 7: imprimir una D
//
//Si está entre 0 y menor a 6: imprimir una F
//
//cualquier otro valor debe imprimir: Valor desconocido
//
//Por ejemplo:
