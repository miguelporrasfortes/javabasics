
import java.util.Scanner;

public class Rectangulo {

    public static void main(String args[]) {

        int firstValue, secondValue;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter a value.");
        firstValue = Integer.parseInt(scanner.nextLine());
        System.out.println("Please, enter another value.");
        secondValue = Integer.parseInt(scanner.nextLine());

        if (firstValue > secondValue) {
            System.out.println("The first value is bigger.");
        } else {
            System.out.println("The second value is bigger");
        }

    }
}
