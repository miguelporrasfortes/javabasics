package com.gm.mundopc;

import com.gm.mundopc.DispositivoEntrada;

public class Teclado extends DispositivoEntrada {

    private int IdTeclado;
    private static int contadorTeclados;

    public Teclado(String tipoEntrada, String marca) {
        super(tipoEntrada, marca);
        this.IdTeclado = ++Teclado.contadorTeclados;
    }

    @Override
    public String toString() {
        return "Teclado{" + "IdTeclado=" + IdTeclado + super.toString() + '}';
    }

}
