package mundopc;

import com.gm.mundopc.*;

public class MundoPc {
    public static void main(String[] args) {
        Raton raton1 = new Raton("entrada ratón 1", "marca ratón 1");
        System.out.println(raton1.toString());
        Teclado teclado1 = new Teclado("entrada teclado 1", "marca teclado 1");
        System.out.println(teclado1.toString());;
        Monitor monitor1 = new Monitor("Marca monitor 1", 5000.00);
        System.out.println(monitor1.toString());;
        Computadora computadora1 = new Computadora("Nombre Computadora 1", monitor1, teclado1, raton1);
        System.out.println(computadora1.toString());;
        Orden orden1 = new Orden();
        orden1.agregarComputadora(computadora1);
        orden1.mostrarOrden();
    }
}
