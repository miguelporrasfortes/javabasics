package boxProject;


public class Box {
    
    int length, width, depth;
    
    public Box(){
        System.out.println("Box initialized without arguments.");
    }
    
    public Box(int length, int width, int depth) {
        this.length = length;
        this.width = width;
        this.depth = depth;
        System.out.println("Box initiliazed with arguments");
    }
    
    public void calculateVolume() {
        int volume = this.length * this.width * this.depth;
        System.out.println("The volume of the Box is: " + volume);
    }
}
