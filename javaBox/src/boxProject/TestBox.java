package boxProject;

public class TestBox {
    public static void main(String[] args) {
        Box box1 = new Box();
        Box box2 = new Box(3, 2, 6);
        
        box1.calculateVolume();
        box2.calculateVolume();
    }
}
