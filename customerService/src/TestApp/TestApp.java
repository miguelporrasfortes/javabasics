package TestApp;

import Cliente.Cliente;
import Empleado.Empleado;
import Persona.Persona;

public class TestApp {
    public static void main(String[] args) {
        Cliente cliente1 = new Cliente();
        Persona persona1 = new Persona("Miguel",'v' , 30);
        Empleado empleado1 = new Empleado();
        
        System.out.println("empleado1 = " + empleado1.getNombre());
        System.out.println("Persona 1 = " + persona1.getEdad());
        System.out.println("cliente1 = " + cliente1.getFechaRegistro());
        
    }
}
