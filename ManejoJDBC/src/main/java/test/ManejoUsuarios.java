package test;

import datos.UsuarioJDBC;
import domain.Usuario;
import java.util.List;


public class ManejoUsuarios {
    public static void main(String[] args) {
        UsuarioJDBC usuarioJdbc = new UsuarioJDBC();
        
        List<Usuario> usuarios = usuarioJdbc.select();
        for(Usuario usuario: usuarios) {
            System.out.println("Usuario: " + usuario);
        }
        
//        Usuario usuario1 = new Usuario();
//        usuario1.setUsername("Max Holloway");
//        usuario1.setPassword("347");
//        System.out.println(usuarioJdbc.insert(usuario1));
        
//        Usuario usuario = new Usuario(2, "Zabit Magomedsharipov", "667");
//        usuarioJdbc.update(usuario);
        
//        usuarioJdbc.delete(new Usuario(3));
    }
}
